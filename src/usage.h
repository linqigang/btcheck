/*
 * usage.h
 *
 * $Id: usage.h 3 2009-09-05 22:37:41Z diraison $
 *
 * This file is part of the btcheck project (c) 2008-2009 distributed
 * under the GNU GPLv3 license and created by Jean Diraison
 * <jean.diraison@ac-rennes.fr>
 *
 * URL: http://sourceforge.net/projects/btcheck/
 *
 */

#ifndef BTCHECK_USAGE_H_
#define BTCHECK_USAGE_H_

void version();
void usage(char *progname, int code);

#endif
