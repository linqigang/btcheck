/*
 * meta.c
 *
 * $Id: meta.c 26 2012-02-19 22:28:17Z diraison $
 *
 * This file is part of the btcheck project (c) 2008-2009 distributed
 * under the GNU GPLv3 license and created by Jean Diraison
 * <jean.diraison@ac-rennes.fr>
 *
 * URL: http://sourceforge.net/projects/btcheck/
 *
 */

#include "config.h"

#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include "btree.h"
#include "bdecode.h"
#include "bencode.h"
#include "hash.h"

#ifndef DIRSEP
#define DIRSEP "/"
#endif

#define INT2HEX(x)	((0 <= (x) && (x) <= 9) ? ((x)+'0') : ((x)-10+'a'))

string_t	TorrentAnnounceSearch[]		= { "announce", NULL };
string_t	TorrentInfoSearch[]		= { "info", NULL };
string_t	TorrentInfoFilesSearch[]	= { "info", "files", NULL };
string_t	TorrentInfoLengthSearch[]	= { "info", "length", NULL };
string_t	TorrentInfoNameSearch[]		= { "info", "name", NULL };
string_t	TorrentInfoPieceLengthSearch[]	= { "info", "piece length", NULL };
string_t	TorrentInfoPiecesSearch[]	= { "info", "pieces", NULL };
string_t	TorrentInfoPrivateSearch[]	= { "info", "private", NULL };

string_t	LengthSearch[]			= { "length", NULL };
string_t	PathSearch[]			= { "path", NULL };

static void print_btree(FILE *file, int c) { fprintf(file, "%c", c); }

static void print_path(FILE *file, btree_t *list)
{
	btree_t *element;
	string_t string;
	int i;
	
	for (i = 0; i < get_btree_length(list); i++) {
		element = get_list_element(list, i);
		if (element != NULL) {
			get_btree_string(element, &string, NULL);
			fprintf(file, i == 0 ? "%s" : DIRSEP "%s", (char *)string);
		} else {
			break;
		}
	}
}

int print_path_map(FILE *file, btree_t *btree)
{
	btree_t *tmpbtree;
	integer_t length;

	if (file == NULL || btree == NULL)
		return -1;
	tmpbtree = search_dict_value(btree, PathSearch);
	if (tmpbtree == NULL) {
		fprintf(stderr, "ERROR : no path attribute found!\n");
		return -1;
	}
	print_path(file, tmpbtree);
	tmpbtree = search_dict_value(btree, LengthSearch);
	if (tmpbtree == NULL) {
		fprintf(stderr, "ERROR : no length attribute found!\n");
		return -1;
	}
	get_btree_integer(tmpbtree, &length);
	fprintf(file, "\t(" LLD_FMT ")\n", (long long)length);
	return 0;
}

int print_element_map(const char *format, btree_t *element)
{
	string_t string;
	int ret;
	
	if (element == NULL || format == NULL)
		return -1;
	ret = get_btree_string(element, &string, NULL);
	if (ret != 0)
		return -1;
	ret = printf(format, (char *)string);
	return (ret >= 0 ? 0 : -1);
}

int print_list_elements_map(const char *format, btree_t *list)
{
	btree_t *element;
	string_t string;
	int ret, i;
	
	if (format == NULL || list == NULL)
		return -1;
/*
	for (i = 0; i < get_btree_length(list); i++) {
		element = get_list_element(list, i);
		if (element == NULL)
			return -1;
		ret = get_btree_string(element, &string, NULL);
		if (ret != 0)
			return -1;
		printf(format, (char *)string);
	}
*/
	ret = map_list(list, (int (*)(void *, btree_t *))print_element_map, (void *)format);
	return ret;
}

int add_length_map(integer_t *totallength, btree_t *btree)
{
	btree_t *tmpbtree;
	integer_t length;
	
	if (totallength == NULL || btree == NULL)
		return -1;
	tmpbtree = search_dict_value(btree, LengthSearch);
	if (tmpbtree == NULL) {
		fprintf(stderr, "ERROR : no length attribute found!\n");
		return -1;
	}
	get_btree_integer(tmpbtree, &length);
	*totallength += length;
	return 0;
}

int get_meta_hash(btree_t *info, unsigned char *hexhash)
{
	unsigned char digest[HASH_LENGTH];
	hashcharctx_t ctx;
	int i, ret, h, l;
	
	ret = init_hash_char(&ctx);
	if (ret == 0) {
		fprintf(stderr, "Can't init hash context.\n");
		return -1;
	}
	bencode(info, (void *)update_hash_char, (void *)&ctx);
	ret = final_hash_char(digest, &ctx);
	if (ret == 0) {
		fprintf(stderr, "Can't get hash result.\n");
		return -1;
	}
	for (i = 0; i < HASH_LENGTH; i++) {
		h = (digest[i] & 0xF0) >> 4;
		l = (digest[i] & 0x0F);
		hexhash[2*i]   = INT2HEX(h);
		hexhash[2*i+1] = INT2HEX(l);
	}
	hexhash[2*HASH_LENGTH] = '\0';
	return 0;	
}

int get_meta_string(btree_t *btree, string_t *info, string_t *string, int *length)
{
	if (btree == NULL || info == NULL || string == NULL)
		return -1;
	btree = search_dict_value(btree, info);
	if (btree == NULL)
		return -1;
	return get_btree_string(btree, string, length);
}

int get_meta_integer(btree_t *btree, string_t *info, integer_t *integer)
{
	if (btree == NULL || info == NULL || integer == NULL)
		return -1;
	btree = search_dict_value(btree, info);
	if (btree == NULL)
		return -1;
	return get_btree_integer(btree, integer);
}
