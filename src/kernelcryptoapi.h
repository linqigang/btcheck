/*
 * kernelcryptoapi.h
 *
 * $Id$
 *
 * This file is part of the btcheck project (c) 2008-2009 distributed
 * under the GNU GPLv3 license and created by Jean Diraison
 * <jean.diraison@ac-rennes.fr>
 *
 * URL: http://sourceforge.net/projects/btcheck/
 *
 */

#ifndef BTCHECK_KERNEL_CRYPTO_API_H_
#define BTCHECK_KERNEL_CRYPTO_API_H_

#include "config.h"

#ifdef WITH_KERNEL_CRYPTO_API

#include <sys/types.h>
#include <sys/socket.h>
#include <linux/if_alg.h>

typedef struct {
	struct sockaddr_alg sa;
	int safd;
	int fd;
} lkca_hash_ctx;

#define KCA_SHA1_DIGEST_LENGTH	20

int lkca_sha1_init(lkca_hash_ctx *ctx);
int lkca_sha1_update(lkca_hash_ctx *ctx, const void *data, unsigned long length);
int lkca_sha1_final(unsigned char *md, lkca_hash_ctx *ctx);

#endif

#endif
