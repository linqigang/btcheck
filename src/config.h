/* src/config.h.  Generated from config.h.in by configure.  */
/* src/config.h.in.  Generated from configure.ac by autoheader.  */

/* Define if building universal (internal helper macro) */
/* #undef AC_APPLE_UNIVERSAL_BUILD */

/* Define canonical host type. */
#define CANONICAL_HOST "i686-pc-linux-gnu"

/* Define to 1 if you have the <beecrypt/sha1.h> header file. */
/* #undef HAVE_BEECRYPT_SHA1_H */

/* Define to 1 if you have the <errno.h> header file. */
#define HAVE_ERRNO_H 1

/* Define to 1 if you have the `fchdir' function. */
#define HAVE_FCHDIR 1

/* Define to 1 if you have the <fcntl.h> header file. */
#define HAVE_FCNTL_H 1

/* Define to 1 if you have the <gcrypt.h> header file. */
/* #undef HAVE_GCRYPT_H */

/* Define to 1 if you have the `getopt' function. */
#define HAVE_GETOPT 1

/* Define to 1 if you have the <gnutls/crypto.h> header file. */
/* #undef HAVE_GNUTLS_CRYPTO_H */

/* Define to 1 if you have the <inttypes.h> header file. */
#define HAVE_INTTYPES_H 1

/* Define to 1 if you have the <limits.h> header file. */
#define HAVE_LIMITS_H 1

/* Define to 1 if you have the <linux/if_alg.h> header file. */
/* #undef HAVE_LINUX_IF_ALG_H */

/* Define to 1 if the system has the type `long long int'. */
#define HAVE_LONG_LONG_INT 1

/* Define to 1 if your system has a GNU libc compatible `malloc' function, and
   to 0 otherwise. */
#define HAVE_MALLOC 1

/* Define to 1 if you have the <mbedtls/sha1.h> header file. */
/* #undef HAVE_MBEDTLS_SHA1_H */

/* Define to 1 if you have the <memory.h> header file. */
#define HAVE_MEMORY_H 1

/* Define to 1 if you have the <nettle/sha1.h> header file. */
/* #undef HAVE_NETTLE_SHA1_H */

/* Define to 1 if you have the <openssl/sha.h> header file. */
/* #undef HAVE_OPENSSL_SHA_H */

/* Define to 1 if your system has a GNU libc compatible `realloc' function,
   and to 0 otherwise. */
#define HAVE_REALLOC 1

/* Define to 1 if you have the <stdint.h> header file. */
#define HAVE_STDINT_H 1

/* Define to 1 if you have the <stdio.h> header file. */
#define HAVE_STDIO_H 1

/* Define to 1 if you have the <stdlib.h> header file. */
#define HAVE_STDLIB_H 1

/* Define to 1 if you have the <strings.h> header file. */
#define HAVE_STRINGS_H 1

/* Define to 1 if you have the <string.h> header file. */
#define HAVE_STRING_H 1

/* Define to 1 if `salg_name' is a member of `struct sockaddr_alg'. */
/* #undef HAVE_STRUCT_SOCKADDR_ALG_SALG_NAME */

/* Define to 1 if you have the <sys/stat.h> header file. */
#define HAVE_SYS_STAT_H 1

/* Define to 1 if you have the <sys/types.h> header file. */
#define HAVE_SYS_TYPES_H 1

/* Define to 1 if you have the <time.h> header file. */
#define HAVE_TIME_H 1

/* Define to 1 if you have the <tomcrypt.h> header file. */
/* #undef HAVE_TOMCRYPT_H */

/* Define to 1 if you have the <unistd.h> header file. */
#define HAVE_UNISTD_H 1

/* Define to 1 if you have the <wincrypt.h> header file. */
/* #undef HAVE_WINCRYPT_H */

/* Define to 1 if you have the <windows.h> header file. */
/* #undef HAVE_WINDOWS_H */

/* Name of package */
#define PACKAGE "btcheck"

/* Define to the address where bug reports for this package should be sent. */
#define PACKAGE_BUGREPORT "Jean Diraison <jean.diraison@ac-rennes.fr>"

/* Define to the full name of this package. */
#define PACKAGE_NAME "btcheck"

/* Define to the full name and version of this package. */
#define PACKAGE_STRING "btcheck 2.1"

/* Define to the one symbol short name of this package. */
#define PACKAGE_TARNAME "btcheck"

/* Define to the home page for this package. */
#define PACKAGE_URL ""

/* Define to the version of this package. */
#define PACKAGE_VERSION "2.1"

/* Define to 1 if you have the ANSI C header files. */
#define STDC_HEADERS 1

/* Version number of package */
#define VERSION "2.1"

/* Use BeeCrypt library to compute hash. */
/* #undef WITH_BEECRYPT */

/* Use libgcrypt crypto library to compute hash. */
/* #undef WITH_GCRYPT */

/* Use GnuTLS library to compute hash. */
/* #undef WITH_GNUTLS */

/* Use Linux Kernel Crypto API to compute hash. */
/* #undef WITH_KERNEL_CRYPTO_API */

/* Use Nettle crypto library to compute hash. */
/* #undef WITH_NETTLE */

/* Use OpenSSL crypto library to compute hash. */
/* #undef WITH_OPENSSL */

/* Use mbedTLS/PolarSSL crypto library to compute hash. */
/* #undef WITH_POLARSSL */

/* Use tomcrypt library to compute hash. */
/* #undef WITH_TOMCRYPT */

/* Use Windows CryptoAPI to compute hash. */
/* #undef WITH_WINDOWS_CRYPTOAPI */

/* Define WORDS_BIGENDIAN to 1 if your processor stores words with the most
   significant byte first (like Motorola and SPARC, unlike Intel). */
#if defined AC_APPLE_UNIVERSAL_BUILD
# if defined __BIG_ENDIAN__
#  define WORDS_BIGENDIAN 1
# endif
#else
# ifndef WORDS_BIGENDIAN
/* #  undef WORDS_BIGENDIAN */
# endif
#endif

/* Enable large inode numbers on Mac OS X 10.5.  */
#ifndef _DARWIN_USE_64_BIT_INODE
# define _DARWIN_USE_64_BIT_INODE 1
#endif

/* Number of bits in a file offset, on hosts where this is settable. */
#define _FILE_OFFSET_BITS 64

/* Define for large files, on AIX-style hosts. */
/* #undef _LARGE_FILES */

/* Define to rpl_malloc if the replacement function should be used. */
/* #undef malloc */

/* Define to rpl_realloc if the replacement function should be used. */
/* #undef realloc */

/* Define to `unsigned int' if <sys/types.h> does not define. */
/* #undef size_t */
