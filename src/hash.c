/*
 * hash.c
 *
 * $Id: hash.c 62 2017-08-28 21:38:50Z diraison $
 *
 * This file is part of the btcheck project (c) 2008-2009 distributed
 * under the GNU GPLv3 license and created by Jean Diraison
 * <jean.diraison@ac-rennes.fr>
 *
 * URL: http://sourceforge.net/projects/btcheck/
 *
 */

#include "config.h"

#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include "hash.h"


#ifdef WITH_BUILTIN_SHA

#ifdef WORDS_BIGENDIAN
#define ENDIAN(w)  (w)
#else
#define ENDIAN(w)  (((w & 0xff) << 24) | ((w & 0xff00) << 8) | ((w & 0xff0000) >> 8) | ((w & 0xff000000) >> 24))
#endif

#define F1(B,C,D)  (D ^ (B & (C ^ D)))
#define F2(B,C,D)  (B ^ C ^ D)
#define F3(B,C,D)  ((B & C) | (D & (B | C)))
#define F4(B,C,D)  (B ^ C ^ D)

#define ROL1(w)    ((w << 1) | ((w >> 31) & 0x1))
#define ROL5(w)    ((w << 5) | ((w >> 27) & 0x1f))
#define ROL30(w)   ((w << 30) | ((w >> 2) & 0x3fffffff))

#define ROLEXCHANGE(a,b,c,d,e,t)  do { e=d; d=c; c=ROL30(b); b=a; a=t; } while (0)

static void SHA1_Compute_block(hashcharctx_t *ctx)
{
	uint32_t a = ctx->hash[0], b = ctx->hash[1], c = ctx->hash[2], d = ctx->hash[3], e = ctx->hash[4];
	uint32_t *block = (uint32_t *) (ctx->buffer);
	uint32_t w[80], t;
	int i;
	
	for (i = 0; i < 16; i++) {
		w[i] = ENDIAN(block[i]);
	}
	for (i = 16; i < 80; i++) {
		t = w[i-3] ^ w[i-8] ^ w[i-14] ^ w[i-16];
		w[i] = ROL1(t);
	}
	for (i = 0; i < 20; i++) {
		t = ROL5(a) + F1(b,c,d) + e + w[i] + 0x5a827999;
		ROLEXCHANGE(a,b,c,d,e,t);
	}
	for (i = 20; i < 40; i++) {
		t = ROL5(a) + F2(b,c,d) + e + w[i] + 0x6ed9eba1;
		ROLEXCHANGE(a,b,c,d,e,t);
	}
	for (i = 40; i < 60; i++) {
		t = ROL5(a) + F3(b,c,d) + e + w[i] + 0x8f1bbcdc;
		ROLEXCHANGE(a,b,c,d,e,t);
	}
	for (i = 60; i < 80; i++) {
		t = ROL5(a) + F4(b,c,d) + e + w[i] + 0xca62c1d6;
		ROLEXCHANGE(a,b,c,d,e,t);
	}
	ctx->hash[0] += a;
	ctx->hash[1] += b;
	ctx->hash[2] += c;
	ctx->hash[3] += d;
	ctx->hash[4] += e;
}

int SHA1_Init(hashcharctx_t *ctx)
{
	if (ctx == NULL)
		return 0;
	ctx->hash[0] = 0x67452301;
	ctx->hash[1] = 0xefcdab89;
	ctx->hash[2] = 0x98badcfe;
	ctx->hash[3] = 0x10325476;
	ctx->hash[4] = 0xc3d2e1f0;
	ctx->length = 0;
	ctx->buflen = 0;
	return 1;
}

int SHA1_Update(hashcharctx_t *ctx, const void *data, unsigned long length)
{
	char *ptr = (char *)data;
	
	ctx->length += length;
	while (length > 0) {
		ctx->buffer[ctx->buflen++] = *(ptr++);
		if (ctx->buflen == 64) {
			SHA1_Compute_block(ctx);
			ctx->buflen = 0;
		}
		length--;
	}
	return 1;
}

int SHA1_Final(unsigned char *md, hashcharctx_t *ctx)
{
	unsigned char pad[1] = { 0x80 };
	unsigned char null[1] = { 0x00 };
	unsigned char len[8];
	int i;
	
	for (i = 0; i < 8; i++)
		len[i] = ((ctx->length << 3) >> (56-8*i)) & 0xff;
	SHA1_Update(ctx, pad, 1);
	while (ctx->buflen != 56)
		SHA1_Update(ctx, null, 1);
	SHA1_Update(ctx, len, 8);
	for (i = 0; i < 5; i++)
		((uint32_t *)md)[i] = ENDIAN(ctx->hash[i]);
	return 1;
}

#endif


#ifdef WITH_POLARSSL

int SHA1_Init(hashcharctx_t *ctx)
{
	mbedtls_sha1_starts(ctx);
	return 1;
}

int SHA1_Update(hashcharctx_t *ctx, const void *data, unsigned long length)
{
	mbedtls_sha1_update(ctx, data, length);
	return 1;
}

int SHA1_Final(unsigned char *md, hashcharctx_t *ctx)
{
	mbedtls_sha1_finish(ctx, md);
	return 1;
}

#endif


#ifdef WITH_GNUTLS

int SHA1_Init(hashcharctx_t *ctx)
{
	int res = gnutls_hash_init(&ctx->dig, GNUTLS_DIG_SHA1);
	return (res ? 0 : 1);
}

int SHA1_Update(hashcharctx_t *ctx, const void *data, unsigned long length)
{
	int res = gnutls_hash(ctx->dig, data, length);
	return (res ? 0 : 1);
}
int SHA1_Final(unsigned char *md, hashcharctx_t *ctx)
{
	gnutls_hash_deinit(ctx->dig, md);
	return 1;
}

#endif


#ifdef WITH_GCRYPT

int SHA1_Init(hashcharctx_t *ctx)
{
	int res = gcry_md_open(&ctx->handler, GCRY_MD_SHA1, 0);
	return (res ? 0 : 1);
}

int SHA1_Update(hashcharctx_t *ctx, const void *data, unsigned long length)
{
	gcry_md_write(ctx->handler, data, length);
	return 1;
}

int SHA1_Final(unsigned char *md, hashcharctx_t *ctx)
{
	unsigned char *hash;

	hash = gcry_md_read(ctx->handler, GCRY_MD_SHA1);
	if (hash != NULL) {
		int i;
		for (i = 0; i < HASH_LENGTH; i++)
			md[i] = hash[i];
	}
	gcry_md_close(ctx->handler);
	return (hash != NULL ? 1 : 0);
}

#endif


#ifdef WITH_TOMCRYPT

int SHA1_Init(hashcharctx_t *ctx)
{
	int res = sha1_init(ctx);
	return (res ? 0 : 1);
}

int SHA1_Update(hashcharctx_t *ctx, const void *data, unsigned long length)
{
	int res = sha1_process(ctx, data, length);
	return (res ? 0 : 1);
}

int SHA1_Final(unsigned char *md, hashcharctx_t *ctx)
{
	int res = sha1_done(ctx, md);
	return (res ? 0 : 1);
}

#endif


#ifdef WITH_NETTLE

int SHA1_Init(hashcharctx_t *ctx)
{
	nettle_sha1_init(ctx);
	return 1;
}

int SHA1_Update(hashcharctx_t *ctx, const void *data, unsigned long length)
{
	nettle_sha1_update(ctx, length, data);
	return 1;
}

int SHA1_Final(unsigned char *md, hashcharctx_t *ctx)
{
	nettle_sha1_digest(ctx, HASH_LENGTH, md);
	return 1;
}

#endif


#ifdef WITH_BEECRYPT

int SHA1_Init(hashcharctx_t *ctx)
{
	int res = sha1Reset(ctx);
	return (res ? 0 : 1);
}

int SHA1_Update(hashcharctx_t *ctx, const void *data, unsigned long length)
{
	int res = sha1Update(ctx, data, length);
	return (res ? 0 : 1);
}

int SHA1_Final(unsigned char *md, hashcharctx_t *ctx)
{
	int res = sha1Digest(ctx, md);
	return (res ? 0 : 1);
}

#endif


#ifdef WITH_KERNEL_CRYPTO_API

int SHA1_Init(hashcharctx_t *ctx)
{
	return lkca_sha1_init(ctx);
}

int SHA1_Update(hashcharctx_t *ctx, const void *data, unsigned long length)
{
	return lkca_sha1_update(ctx, data, length);
}

int SHA1_Final(unsigned char *md, hashcharctx_t *ctx)
{
	return lkca_sha1_final(md, ctx);
}

#endif


#ifdef WITH_WINDOWS_CRYPTOAPI

int SHA1_Init(hashcharctx_t *ctx)
{
	ctx->hCryptProv = (HCRYPTPROV) NULL;
	ctx->hCryptHash = (HCRYPTHASH) NULL;
	if ( ! CryptAcquireContext(&ctx->hCryptProv, NULL, NULL, PROV_RSA_FULL, CRYPT_VERIFYCONTEXT | CRYPT_SILENT) )
		return 0;
	if ( ! CryptCreateHash(ctx->hCryptProv, CALG_SHA1, 0, 0, &ctx->hCryptHash) )
		return 0;
	return 1;
}

int SHA1_Update(hashcharctx_t *ctx, const void *data, unsigned long length)
{
	BOOL res;
	
	res = CryptHashData(ctx->hCryptHash, (BYTE *)data, length, 0);
	return (res ? 1 : 0);
}

int SHA1_Final(unsigned char *md, hashcharctx_t *ctx)
{
	DWORD hashlen = SHA_DIGEST_LENGTH;
	BOOL res;
	
	res = CryptGetHashParam(ctx->hCryptHash, HP_HASHVAL, (BYTE *)md, &hashlen, 0);
	if ( ctx->hCryptHash ) {
		CryptDestroyHash(ctx->hCryptHash);
		ctx->hCryptHash = (HCRYPTHASH) NULL;
	}
	if ( ctx->hCryptProv ) {
		CryptReleaseContext(ctx->hCryptProv, 0);
		ctx->hCryptProv = (HCRYPTPROV) NULL;
	}
	return (res ? 1 : 0);
}

#endif


#ifndef WITH_OPENSSL

unsigned char *SHA1(void *data, unsigned long length, unsigned char *md)
{
	hashcharctx_t ctx;

	SHA1_Init(&ctx);
	SHA1_Update(&ctx, data, length);
	SHA1_Final(md, &ctx);
	return md;
}

#endif


int init_hash_char(hashcharctx_t *ctx)
{
	return SHA1_Init(ctx);
}

int update_hash_char(hashcharctx_t *ctx, int c)
{
	char data[1];
	data[0] = (char)c;
	return SHA1_Update(ctx, data, 1);
}

int final_hash_char(unsigned char *md, hashcharctx_t *ctx)
{
	return SHA1_Final(md, ctx);
}

unsigned char *compute_hash_buffer(unsigned char *md, void *data, unsigned long length)
{
	return SHA1(data, length, md);
}
