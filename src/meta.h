/*
 * meta.h
 *
 * $Id: meta.h 3 2009-09-05 22:37:41Z diraison $
 *
 * This file is part of the btcheck project (c) 2008-2009 distributed
 * under the GNU GPLv3 license and created by Jean Diraison
 * <jean.diraison@ac-rennes.fr>
 *
 * URL: http://sourceforge.net/projects/btcheck/
 *
 */

#ifndef BTCHECK_META_H_
#define BTCHECK_META_H_

#include <stdio.h>
#include "btree.h"

extern string_t	TorrentAnnounceSearch[];
extern string_t	TorrentInfoSearch[];
extern string_t	TorrentInfoFilesSearch[];
extern string_t	TorrentInfoLengthSearch[];
extern string_t	TorrentInfoNameSearch[];
extern string_t	TorrentInfoPieceLengthSearch[];
extern string_t	TorrentInfoPiecesSearch[];
extern string_t	TorrentInfoPrivateSearch[];

extern string_t	LengthSearch[];
extern string_t	PathSearch[];

int print_path_map(FILE *file, btree_t *btree);
int print_element_map(const char *format, btree_t *element);
int print_list_elements_map(const char *format, btree_t *list);
int add_length_map(integer_t *totallength, btree_t *btree);
int get_meta_hash(btree_t *info, unsigned char *hexhash);
int get_meta_string(btree_t *btree, string_t *info, string_t *string, int *length);
int get_meta_integer(btree_t *btree, string_t *info, integer_t *integer);
void get_info(btree_t *torrentbtree);

#endif
