/*
 * info.c
 *
 * $Id: info.c 44 2017-08-22 23:14:11Z diraison $
 *
 * This file is part of the btcheck project (c) 2008-2009 distributed
 * under the GNU GPLv3 license and created by Jean Diraison
 * <jean.diraison@ac-rennes.fr>
 *
 * URL: http://sourceforge.net/projects/btcheck/
 *
 */

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <time.h>

#include "btcheck.h"
#include "btree.h"
#include "meta.h"
#include "hash.h"

string_t	TorrentAnnounceListSearch[]	= { "announce-list", NULL };
string_t	TorrentCreationDateSearch[]	= { "creation date", NULL };
string_t	TorrentCreatedBySearch[]	= { "created by", NULL };
string_t	TorrentCommentSearch[]		= { "comment", NULL };
string_t	TorrentUrllistSearch[]		= { "url-list", NULL };

static void print_extended_info(btree_t *torrentbtree)
{
	string_t torrentcreatedby, torrentcomment;
	integer_t torrentcreationdate;
	time_t time;
	int ret;
	
	/* Creation date */
	torrentcreationdate = 0;
	ret = get_meta_integer(torrentbtree, TorrentCreationDateSearch, &torrentcreationdate);
	if (ret == 0 && torrentcreationdate != 0) {
		time = (time_t)torrentcreationdate;
		if (time == torrentcreationdate)
			printf("Creat. Date  : %s", ctime(&time));
	}
	/* Created by */
	torrentcreatedby = NULL;
	ret = get_meta_string(torrentbtree, TorrentCreatedBySearch, &torrentcreatedby, NULL);
	if (ret == 0 && torrentcreatedby != NULL)
		printf("Created By   : %s\n", torrentcreatedby);
	/* Comment */
	torrentcomment = NULL;
	ret = get_meta_string(torrentbtree, TorrentCommentSearch, &torrentcomment, NULL);
	if (ret == 0 && torrentcomment != NULL && strlen(torrentcomment) != 0)
		printf("Comment      : %s\n", torrentcomment);
	return;
}

static void print_announce_list(btree_t *torrentbtree)
{
	btree_t	*torrentannouncelist;
	int ret;
	
	torrentannouncelist = search_dict_value(torrentbtree, TorrentAnnounceListSearch);
	if (torrentannouncelist != NULL) {
		ret = map_list(torrentannouncelist, (int (*)(void *, btree_t *))print_list_elements_map, "    announce : %s\n");
		if (ret != 0) {
			fprintf(stderr, "Can't print announce list items.\n");
			exit(EXIT_FAILURE);
		}
	}
	return;
}

static void print_urllist_list(btree_t *torrentbtree)
{
	btree_t	*torrenturllist;
	string_t webseed;
	int ret, strlen;
	
	torrenturllist = search_dict_value(torrentbtree, TorrentUrllistSearch);
	if (torrenturllist != NULL) {
		ret = get_btree_string(torrenturllist, &webseed, &strlen);
		if (ret == 0) {
			if (strlen > 0)
				printf("    web seed : %s\n", webseed);
		} else {
			ret = map_list(torrenturllist, (int (*)(void *, btree_t *))print_element_map, "    web seed : %s\n");
			if (ret != 0) {
				fprintf(stderr, "Can't print url-list list items.\n");
				exit(EXIT_FAILURE);
			}
		}
	}
	return;	
}

void print_info(btree_t *torrentbtree)
{
	btree_t	*torrentinfo, *torrentinfofiles;
	string_t torrentinfoname, torrentannounce;
	integer_t torrentinfolength, torrentinfopiecelength, torrentinfoprivate;
	unsigned char	torrentdigest[HASH_LENGTH*2+1];
	int ret, issinglefile, isprivate;
	
	/* Announce URL */
	torrentannounce = NULL;
	ret = get_meta_string(torrentbtree, TorrentAnnounceSearch, &torrentannounce, NULL);
	if (ret != 0) {
		fprintf(stderr, "Can't get announce!\n");
	}
	/* Info Private */
	torrentinfoprivate = 0;
	get_meta_integer(torrentbtree, TorrentInfoPrivateSearch, &torrentinfoprivate);
	isprivate = torrentinfoprivate == 0 ? 0 : 1;
	/* Piece Length */
	torrentinfopiecelength = 0;
	ret = get_meta_integer(torrentbtree, TorrentInfoPieceLengthSearch, &torrentinfopiecelength);
	if (ret != 0) {
		fprintf(stderr, "Can't get piece length!\n");
	}
	/* Name */
	torrentinfoname = NULL;
	ret = get_meta_string(torrentbtree, TorrentInfoNameSearch, &torrentinfoname, NULL);
	if (ret != 0) {
		fprintf(stderr, "Can't get name!\n");
	}
	/* Length */
	torrentinfolength = 0;
	torrentinfofiles = search_dict_value(torrentbtree, TorrentInfoFilesSearch);
	if (torrentinfofiles == NULL) {			/* single file */
		ret = get_meta_integer(torrentbtree, TorrentInfoLengthSearch, &torrentinfolength);
		if (ret != 0) {
			fprintf(stderr, "Can't get length.\n");
			exit(EXIT_FAILURE);
		}
		issinglefile = 1;
	} else {					/* multi file */
		ret = map_list(torrentinfofiles, (int (*)(void *, btree_t *))add_length_map, &torrentinfolength);
		if (ret != 0) {
			fprintf(stderr, "Can't compute total length.\n");
			exit(EXIT_FAILURE);
		}
		issinglefile = 0;
		/* dump_btree(torrentinfofiles, 0); */
	}
	/* Hash */
	torrentinfo = search_dict_value(torrentbtree, TorrentInfoSearch);
	if (torrentinfo == NULL) {
		fprintf(stderr, "Can't get info, torrent corrupted!\n");
		exit(EXIT_FAILURE);
	}
	/* bencode(torrentinfo, (void *)printbtree, (void *)stdout); */
	ret = get_meta_hash(torrentinfo, torrentdigest);
	if (ret != 0) {
		fprintf(stderr, "Hash computing error.\n");
		exit(EXIT_FAILURE);
	}
	
	printf(isprivate ? "Announce URL : %s (private)\n" : "Announce URL : %s\n", (char *)torrentannounce);
	if (Verbose) {
		print_announce_list(torrentbtree);
		print_urllist_list(torrentbtree);
	}
	printf(issinglefile ? "File Name    : %s\n"   : "Dir. Name    : %s\n", (char *)torrentinfoname);
	printf(issinglefile ? "File Length  : " LLD_FMT "\n" : "Total Length : " LLD_FMT "\n", (long long)torrentinfolength);
	printf("Piece Length : " LLD_FMT "\n", (long long)torrentinfopiecelength);
	printf("Torrent Hash : %s\n", torrentdigest);
	
	if (Verbose)
		print_extended_info(torrentbtree);
	
	return;
}
