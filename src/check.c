/*
 * check.c
 *
 * $Id: check.c 56 2017-08-26 19:37:38Z diraison $
 *
 * This file is part of the btcheck project (c) 2008-2009 distributed
 * under the GNU GPLv3 license and created by Jean Diraison
 * <jean.diraison@ac-rennes.fr>
 *
 * URL: http://sourceforge.net/projects/btcheck/
 *
 */

#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include "btcheck.h"
#include "btree.h"
#include "meta.h"
#include "file.h"
#include "hash.h"

#ifndef MIN
#define MIN(x,y)	((x) < (y) ? (x) : (y))
#endif

int check(btree_t *torrentbtree)
{
	btree_t *torrentinfo, *torrentinfofiles;
	integer_t torrentinfopiecelength, torrentinfolength;
	string_t torrentinfopieces;
	int torrentinfopiecessize;
	unsigned char digest[HASH_LENGTH];
	integer_t readlength;
	btfile_t *btfile;
	void *piecebuffer, *piecedigest;
	size_t size, length;
	int ret, pieceindex, failure;
		
	torrentinfo = search_dict_value(torrentbtree, TorrentInfoSearch);
	if (torrentinfo == NULL) {
		fprintf(stderr, "Can't get info, torrent corrupted!\n");
		exit(EXIT_FAILURE);
	}
	ret = get_meta_integer(torrentbtree, TorrentInfoPieceLengthSearch, &torrentinfopiecelength);
	if (ret != 0) {
		fprintf(stderr, "Can't get piece length!\n");
		exit(EXIT_FAILURE);
	}
	ret = get_meta_string(torrentbtree, TorrentInfoPiecesSearch, &torrentinfopieces, &torrentinfopiecessize);
	if (ret != 0) {
		fprintf(stderr, "Can't get pieces!\n");
		exit(EXIT_FAILURE);
	}
	torrentinfofiles = search_dict_value(torrentbtree, TorrentInfoFilesSearch);
	if (torrentinfofiles != NULL) {
		torrentinfolength = 0;
		ret = map_list(torrentinfofiles, (int (*)(void *, btree_t *))add_length_map, &torrentinfolength);
		if (ret != 0) {
			fprintf(stderr, "Can't compute total length.\n");
			exit(EXIT_FAILURE);
		}	
	} else {
		ret = get_meta_integer(torrentbtree, TorrentInfoLengthSearch, &torrentinfolength);
		if (ret != 0) {
			fprintf(stderr, "Can't get total length.\n");
			exit(EXIT_FAILURE);
		}
	}
	
	piecebuffer = malloc(torrentinfopiecelength);
	if (piecebuffer == NULL) {
		fprintf(stderr, "Can't allocate memory to store one piece.\n");
		exit(EXIT_FAILURE);
	}
	
	btfile = open_btfile(torrentbtree);
	if (btfile == NULL) {
		fprintf(stderr, "Can't open btfile.\n");
	}
	
	failure = 0;
	pieceindex = 0;
	readlength = 0;
	while (readlength < torrentinfolength) {
		length = MIN(torrentinfopiecelength, torrentinfolength-readlength);
		size = read_btfile(btfile, piecebuffer, length);
		if (size != READERR_HOLES) {
			if (size == 0 || size != length) {
				fprintf(stderr, "Can't read btfile.\n");
				exit(EXIT_FAILURE);
			}
			compute_hash_buffer(digest, piecebuffer, length);
			piecedigest = ((void *)torrentinfopieces) + HASH_LENGTH * pieceindex;
			ret = memcmp(piecedigest, digest, HASH_LENGTH);
		} else {
			ret = -1;
		}	
		if (ret == 0) {
			if (Verbose >= 0)
				printf("\rpiece[%d] OK ", pieceindex);
		} else {
			if (Verbose >= 0)
				printf("\rpiece[%d] BAD \n", pieceindex);
			failure++;
		}
		fflush(stdout);
		readlength += length;
		pieceindex++;
	}

	close_btfile(btfile);
	
	if (failure == 0) {
		if (Verbose >= 0)
			printf("\rData integrity checked, all is ok.\n");
	} else {
		if (Verbose >= 0)
			printf("\rData integrity failure, %d errors found!\n", failure);
	}
	
	return failure;
}
