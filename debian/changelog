btcheck (2.1-5) unstable; urgency=medium

  * New maintainer (Closes: #986930)
  * debian/compat: Increased compat level from 11 to 13
  * debian/control: Specified rules do not require root
  * debian/control: Replaced compat file with debhelper-compat
  * debian/copyright: Updated copyright
  * debian/tests/control: Added superficial restriction to test
  * debian/upstream/metadata: Added upstream metadata

 -- Lin Qigang <lqi254@protonmail.com>  Wed, 8 Sep 2021 23:22:28 +0700

btcheck (2.1-4) unstable; urgency=medium

  [ Ondřej Nový ]
  * d/copyright: Use https protocol in Format field
  * d/watch: Use https protocol

  [ Herbert Fortes ]
  * debian/control:
      - Bump Standards-Version from 4.1.2 to 4.2.1
  * debian/copyright:
      - Update year for maintainer
  * debian/rules:
      - Add DH_VERBOSE
      - Remove '--with autoreconf'

 -- Herbert Parentes Fortes Neto <hpfn@debian.org>  Tue, 13 Nov 2018 10:31:45 -0200

btcheck (2.1-3) unstable; urgency=medium

  * debian/tests/control:
      - Add 'allow-stderr'

 -- Herbert Parentes Fortes Neto <hpfn@debian.org>  Thu, 28 Dec 2017 08:32:47 -0200

btcheck (2.1-2) unstable; urgency=medium

  * DH_LEVEL: 11
  * debian/control:
      - Bump Standards-Version from 4.1.0 to 4.1.2
      - Update Vcs-* fields to salsa.debian.org
  * debian/tests/:
      - Add the control file - CI

 -- Herbert Parentes Fortes Neto <hpfn@debian.org>  Wed, 27 Dec 2017 11:44:37 -0200

btcheck (2.1-1) unstable; urgency=medium

  * New upstream version 2.1
  * DH_LEVEL: 10
  * debian/control:
      - Bump STD-V to 4.1.0
  * debian/copyright:
      - Update
  * debian/gbp.conf:
      - File created
  * debian/patches:
      - Remove fix-spell-bencode-c.patch
      - Remove hyphen-manpage.patch
      - Refresh fix-spell-usage-c.patch

 -- Herbert Parentes Fortes Neto <hpfn@debian.org>  Sun, 03 Sep 2017 15:34:32 -0300

btcheck (1.9-3) unstable; urgency=medium

  * debian/control:
      - Update my email.
      - Bump Standards-Version from 3.9.6 to 3.9.8.
      - Vcs* updated and with https.
  * debian/copyright:
      - Update my email.
  * debian/patches:
      - Refresh fix-spell-usage-c.patch.
      - Refresh hyphen-manpage.patch

 -- Herbert Parentes Fortes Neto <hpfn@debian.org>  Sat, 05 Nov 2016 11:49:56 -0200

btcheck (1.9-2) unstable; urgency=medium

  * debian/copyright: license is GPL3. Without the plus.

 -- Herbert Parentes Fortes Neto <hpfn@ig.com.br>  Sun, 26 Jul 2015 10:59:46 -0300

btcheck (1.9-1) unstable; urgency=medium

  * New upstream release.
  * debian/copyright update.
  * debian/watch updated.

 -- Herbert Parentes Fortes Neto <hpfn@ig.com.br>  Tue, 21 Jul 2015 10:57:53 -0300

btcheck (1.8-1) unstable; urgency=low

  * Initial release (Closes: #783764)

 -- Herbert Parentes Fortes Neto <hpfn@ig.com.br>  Thu, 09 Apr 2015 11:18:26 -0300
